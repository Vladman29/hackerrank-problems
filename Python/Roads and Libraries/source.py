#!/bin/python

import sys

def roadsAndLibraries(n, c_lib, c_road, cities):
    # Complete this function
    graph = {}
    viz = []
    for i in range(1,n+1):
        graph[i] = []
        viz.append(-1)
    viz.append(-1)
    for (x,y) in cities:
        graph[x].append(y)
        graph[y].append(x)
    index = 0
    ans = 0
    for i in range(1,n+1):
        if viz[i] == -1:
            nums = 0
            q = [i]
            viz[i] = index
            while len(q) > 0:
                top = q[0]
                for x in graph[top]:
                    if viz[x] == -1:
                        q.append(x)
                        viz[x] = index
                q.pop(0)
                nums += 1
            ans = ans + c_lib
            ans = ans + min(c_lib, c_road) * (nums-1)
            index += 1
    return ans
    
    

if __name__ == "__main__":
    q = int(raw_input().strip())
    for a0 in xrange(q):
        n, m, c_lib, c_road = raw_input().strip().split(' ')
        n, m, c_lib, c_road = [int(n), int(m), int(c_lib), int(c_road)]
        cities = []
        for cities_i in xrange(m):
            cities_temp = map(int,raw_input().strip().split(' '))
            cities.append(cities_temp)
        result = roadsAndLibraries(n, c_lib, c_road, cities)
        print result

