#!/bin/python3

import sys
def largestRectangle(h):
    n = len(h)
    st = []
    ans = 0
    st.append((h[0], 0))
    for i in range(1,n):
        if h[i] > st[-1][0]:
            st.append((h[i],i))
        else:
            curr = i
            while len(st) > 0 and h[i] < st[-1][0]:
                ans = max(ans, st[-1][0] * (i - st[-1][1]))
                curr = st[-1][1]
                st = st[:-1]
            st.append((h[i], curr))
    while(len(st) > 0):
        ans = max(ans, st[-1][0] * (n - st[-1][1]))
        st = st[:-1]
    return ans
                
    # Complete this function

if __name__ == "__main__":
    n = int(input().strip())
    h = list(map(int, input().strip().split(' ')))
    result = largestRectangle(h)
    print(result)
