#!/bin/python3

import sys

def angryChildren(k, arr):
    # Complete this function
    arr = sorted(arr)
    n = len(arr)
    ans = (10**9) + 1
    for i in range(n-k+1):
        ans = min (ans, arr[i+k-1]-arr[i])
    return ans
        

if __name__ == "__main__":
    n = int(input().strip())
    k = int(input().strip())
    arr = []
    arr_i = 0
    for arr_i in range(n):
        arr_t = int(input().strip())
        arr.append(arr_t)
    result = angryChildren(k, arr)
    print(result)
