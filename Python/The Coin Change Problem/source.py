#!/bin/python

import sys
#import numpy

def getWays(n, c):
    # Complete this function
    m = len(c)
    d = []
    for i in range(m):
        d.append([])
        for j in range(n+1):
            d[i].append(0)
    for i in range(m):
        d[i][0] = 1
    for i in range(1,n+1):
        if i % c[0] == 0:
            d[0][i] = 1
        else:
            d[0][i] = 0
    for i in range(1,m):
        for j in range(1,n+1):
            if j < c[i]:
                d[i][j] = d[i-1][j]
            else:
                d[i][j] = d[i-1][j] + d[i][j-c[i]]
    print(d[m-1][n])
        

n, m = raw_input().strip().split(' ')
n, m = [int(n), int(m)]
c = map(long, raw_input().strip().split(' '))
# Print the number of ways of making change for 'n' units using coins having the values given by 'c'
ways = getWays(n, c)
