# Enter your code here. Read input from STDIN. Print output to STDOUT
from __future__ import print_function
def positive(x):
    return x>0

q = int(raw_input())
for qc in range(q):
    n = int(raw_input())
    arr = map(int,raw_input().split())
    curr_s = 0
    max_s = 0
    for x in arr:
        curr_s += x
        if (curr_s < 0):
            curr_s = 0
            continue
        if (curr_s > max_s):
            max_s = curr_s
    if max_s == 0:
        print(max(arr),max(arr),sep = " ")
    else:
        print(max_s,sum(filter(positive,arr)),sep = " ")