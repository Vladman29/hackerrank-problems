#!/bin/python

import sys

def pairs(k, arr):
    # Complete this function
    arr = sorted(arr)
    total = 0
    i = 0
    j = 1
    N = len(arr)
    while (j < N):
        if arr[i] + k == arr[j]:
            total += 1
            j += 1
        elif arr[i] + k < arr[j]:
            i += 1
        elif arr[i] + k > arr[j]:
            j += 1
    return total
        

if __name__ == "__main__":
    n, k = raw_input().strip().split(' ')
    n, k = [int(n), int(k)]
    arr = map(int, raw_input().strip().split(' '))
    result = pairs(k, arr)
    print result
