# Enter your code here. Read input from STDIN. Print output to STDOUT
#from __future__ import division

def count1bits(a,x):
    ans = 0
    for val in a:
        val = val >> x
        ans += val % 2
    return ans

t = int(raw_input())
for x0 in range(t):
    n = int(raw_input())
    a = map(int, raw_input().split())
    s = 0
    for i in range(32):
        n1 = count1bits(a, i)
        if n1 > 0:
            s = (s + (1<<(n+i-1))) % (10**9 + 7)
    print(s)