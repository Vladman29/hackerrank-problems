#!/bin/python

import sys

d = []

def optimal_length(n):
    if n%5 == 0:
        return n/5
    elif n%5 <= 2:
        return n/5 + 1
    else:
        return n/5 + 2

def equal(arr):
    m = min(arr)
    s1 = 0
    for x in arr:
        s1 = s1 + optimal_length(x-m)
    s2 = 0
    for x in arr:
        s2 = s2 + optimal_length(x+1-m)
    s3 = 0
    for x in arr:
        s3 = s3 + optimal_length(x+2-m)
    return min([s1,s2,s3])
    # Complete this function

if __name__ == "__main__":
    t = int(raw_input().strip())
    for a0 in xrange(t):
        n = int(raw_input().strip())
        arr = map(int, raw_input().strip().split(' '))
        result = equal(arr)
        print result

