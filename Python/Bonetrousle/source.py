# Enter your code here. Read input from STDIN. Print output to STDOUT
t0 = int( raw_input().strip())
for x in range(t0):
    n,k,b = map(int, raw_input().strip().split())
    minim  = b * (b+1) / 2
    maxim  = b * (2*k - b + 1) / 2
    if  n >= minim and n <= maxim:
        num1 = (n - minim) / b
        num2 = (n - minim) % b
        ans = []
        for i in range(1,b+1):
            if (i > b - num2):
                ans.append(i+num1+1)
            else:
                ans.append(i+num1)
        print(' '.join(map(str,ans)))
    else:
        print(-1)