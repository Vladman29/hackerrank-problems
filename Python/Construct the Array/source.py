#!/bin/python3

import sys

def genPerm(n,k,arr):
    if len(arr) == n:
        if arr[-1] == 1:
            return 1
        else:
            return 0
    else:
        ans = 0
        for i in range(k):
            if i != arr[-1]:
                arr.append(i)
                ans = ans + genPerm(n,k,arr)
                arr.pop(-1)
        return ans


def countArray(n, k, x):
    l1 = 0
    l2 = 1
    for i in range(1, n-1):
        old_l2 = l2
        l2 = (l1 + l2 * (k-2)) % (10**9 + 7)
        l1 = (old_l2 * (k-1)) % (10**9 + 7) 
        
    if x == 1:
        return (l1 % (10**9 + 7))
    else:
        return (l2 % (10**9 + 7))
    
    
    # Return the number of ways to fill in the array.

if __name__ == "__main__":
    n, k, x = input().strip().split(' ')
    n, k, x = [int(n), int(k), int(x)]
    answer = countArray(n, k, x)
    print(answer)
