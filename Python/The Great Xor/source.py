#!/bin/python

import sys

def theGreatXor(x):
    power = 0
    ans = 0
    while (x > 0):
        if (x % 2 == 0):
            ans = ans + 2 ** power
        power += 1
        x = x / 2
    return ans
            
            
    # Complete this function

q = int(raw_input().strip())
for a0 in xrange(q):
    x = long(raw_input().strip())
    result = theGreatXor(x)
    print(result)

