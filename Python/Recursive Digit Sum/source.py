#!/bin/python

import sys

def digitSum(n, k):
    # Complete this function
    ans = (int(n)*k)%9
    if ans  == 0:
        ans += 9
    return ans

if __name__ == "__main__":
    n, k = raw_input().strip().split(' ')
    n, k = [str(n), int(k)]
    result = digitSum(n, k)
    print result
