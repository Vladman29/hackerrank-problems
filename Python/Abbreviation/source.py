#!/bin/python

import sys
global dic

def isLowerCase(x):
    if x.lower() != x:
            return False
    return True

def isUpperCase(x):
    if x.upper() != x:
            return False
    return True

def abbreviation(a, b):
    # Complete this function
    d = []
    for i in range(len(b)+1):
        d.append([])
        for j in range(len(a)+1):
            if i==0 and j==0:
                d[0].append(True)
            elif j==0:
                d[i].append(False)
            elif i==0:
                if isUpperCase(a[j-1]):
                    d[0].append(False)
                else:
                    d[0].append(d[0][j-1])
            else:
                if isUpperCase(a[j-1]):
                    if(a[j-1] != b[i-1]):
                        d[i].append(False)
                    else:
                        d[i].append(d[i-1][j-1])
                else:
                    if(a[j-1].upper() == b[i-1]):
                        d[i].append(d[i-1][j-1] or d[i][j-1])
                    else:
                        d[i].append(d[i][j-1])
    return d[len(b)][len(a)]
                        
        
    
    

if __name__ == "__main__":
    global dic
    q = int(raw_input().strip())
    for a0 in xrange(q):
        a = raw_input().strip()
        b = raw_input().strip()
        dic = {}
        if isUpperCase(b):
            result = abbreviation(a, b)
        else:
            if a.upper() != b.upper():
                result = False
            else:
                result = True
                for i,x in enumerate(a):
                    if isUpperCase(a[i]) and isLowerCase(b[i]):
                        result = False         
        if result:
            print("YES")
        else:
            print("NO")

