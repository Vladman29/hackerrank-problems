#!/bin/python3
from collections import Counter
import sys

def isValid(s):
    c = Counter(s)
    values = []
    for key in c.keys():
        values.append(c[key])
    c2 = Counter(values)
    if len(c2.keys()) == 1:
        return "YES"
    if len(c2.keys()) > 2:
        return "NO"
    M = values[0]
    m = values[0]
    F = 1
    f = 1
    for x in values[1:]:
        if x > M:
            M = x
            F = 1
        elif x == M:
            F += 1
        if x < m:
            m = x
            f = 1
        elif x == m:
            f += 1
    if f == 1:
        return"YES"
    elif M - m == 1 and F == 1:
        return "YES"
    else:
        return "NO"
        
    # Complete this function

s = input().strip()
result = isValid(s)
print(result)
