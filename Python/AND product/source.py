#!/bin/python

import sys

def andProduct(a, b):
    ans = 0
    for i in range(31,-1,-1):
        if ((a>>i) == (b>>i)):
            continue
        else:
            mark = i
            break
    return (((a>>i) & (b>>i)) * (1<<i))
    # Complete this function

if __name__ == "__main__":
    n = int(raw_input().strip())
    for a0 in xrange(n):
        a, b = raw_input().strip().split(' ')
        a, b = [long(a), long(b)]
        result = andProduct(a, b)
        print result

