#!/bin/python
import sys

def commonChild(s1, s2):
    n = len(s1)
    m = len(s2)
    d = []
    for i in range(n+1):
        d.append(0)
    for i in range(1,n+1):
        newd = [0]
        for j in range(1,m+1):
            if s1[i-1] == s2[j-1]:
                newd.append(d[j-1] + 1)
            else:
                newd.append(max(d[j],newd[j-1]))
        d = newd
    return d[n]
    # Complete this function

s1 = raw_input().strip()
s2 = raw_input().strip()
result = commonChild(s1, s2)
print(result)
