#!/bin/python3

import os
import sys
import heapq
#
# Complete the cookies function below.
#
def cookies(k, A):
    #
    # Write your code here.
    #
    heapq.heapify(A)
    ans = 0
    while(len(A)>1):
        x1 = A[0]
        if x1 < k:
            heapq.heappop(A)
            x2 = A[0]
            heapq.heappop(A)
            heapq.heappush(A, x1 + 2 * x2)
            ans += 1
        else:
            break
    if A[0] < k:
        return -1
    else:
        return ans

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    nk = input().split()

    n = int(nk[0])

    k = int(nk[1])

    A = list(map(int, input().rstrip().split()))

    result = cookies(k, A)

    fptr.write(str(result) + '\n')

    fptr.close()
