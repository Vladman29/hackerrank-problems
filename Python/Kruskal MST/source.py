#!/bin/python3

import os
import sys


def cmp_edges(A, B):
    if A[1] < B[1]:
        return 1
    else:
        return -1

if __name__ == '__main__':
    n, m = map(int, input().split())
    edges = []
    sets = {}
    s = {}
    
    for i in range(1,n+1):
        sets[i] = [i]
        s[i] = i
    
    for i in range(m):
        a, b, c = map(int, input().split())
        edges.append(((a,b), c))
    
    edges = sorted(edges, key=lambda edge: edge[1])
    
    ans = 0
    for edge in edges:
        (x,y) = edge[0]
        if not s[x] == s[y]:
            ans = ans + edge[1]
            if len(sets[s[x]]) < len(sets[s[y]]):
                for element in sets[s[x]]:
                    sets[s[y]].append(element)
                    s[element] = s[y]
            else:
                for element in sets[s[y]]:
                    sets[s[x]].append(element)
                    s[element] = s[x]
                    
    print(ans)

    #
    # Write your code here.
    #
