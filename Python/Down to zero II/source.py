#!/bin/python3

import os
import sys

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    q1 = int(input())
    
    d = [-1] * 1000001
    d[1] = 1
    d[2] = 2
    d[3] = 3
    d[0] = 0
    for i in range(1,1000001):
        if d[i] == -1 or d[i] > d[i-1] + 1:
            d[i] = d[i-1] + 1
        for j in range(1,i+1):
            if i * j > 1000000:
                break
            if d[i*j] == -1:
                d[i*j] = d[i] + 1
            else:
                d[i*j] = min(d[i]+1, d[i*j])
    for q_itr in range(q1):
        n = int(input())

        result = d[n]

        fptr.write(str(result) + '\n')

    fptr.close()
