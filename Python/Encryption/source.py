#!/bin/python

import sys
import math

def encryption(s):
    words = s.split(" ")
    s = ""
    for word in words:
        s = s + word
    N = len(s)
    l = pow(N,0.5)
    n = int(math.floor(l))
    m = int(math.ceil(l))
    ans = ""
    i = 0
    for j in range(m):
        i = j
        while (i < N):
            ans = ans + s[i]
            i += m
        ans = ans + " "
    return ans
    
    # Complete this function

if __name__ == "__main__":
    s = raw_input().strip()
    result = encryption(s)
    print result
