#!/bin/python3

import sys

def angryChildren(k, packets):
    # Complete this function
    n = len(packets)
    packets = sorted(packets)
    sums = []
    sums.append(sum(packets[1:k]))
    for i in range(k,n):
        sums.append(sums[-1] + packets[i] - packets[i-k+1])
    #print(sums)
    firstU = 0
    coeff = -k+1
    for i in range(k):
        firstU = firstU + coeff * packets[i]
        coeff = coeff + 2
    #print(firstU)
    ans = firstU
    lastU = firstU
    for i in range(k,n):
        #print(lastU, sums[i-k], k-1, packets[i]-packets[i-k])
        lastU = lastU - 2 * sums[i-k] + (k-1) * (packets[i] + packets[i-k])
        #print(lastU)
        ans = min(ans, lastU)
    return ans

if __name__ == "__main__":
    N = int(input().strip())
    K = int(input().strip())
    packets = []
    packets_i = 0
    for packets_i in range(N):
        packets_t = int(input().strip())
        packets.append(packets_t)
    result = angryChildren(K, packets)
    print(result)