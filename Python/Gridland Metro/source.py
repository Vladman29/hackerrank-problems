#!/bin/python

import sys

def gridlandMetro(n, m, k, tracks):
    # Complete this function
    dic = {}
    total = 0
    for (r, c1, c2) in tracks:
        if r in dic.keys():
            dic[r].append((c1,c2))
        else:
            dic[r] = [(c1,c2)]
    keys = dic.keys()
    for k in keys:
        tracks = sorted(dic[k])
        start = tracks[0][0]
        end = tracks[0][1]
        for x in tracks[1:]:
            if x[0] > end:
                total = total + end - start + 1
                start = x[0]
                end = x[1]
                continue
            else:
                end = max(end, x[1])
        total = total + end - start + 1
    return m*n - total

if __name__ == "__main__":
    n, m, k = raw_input().strip().split(' ')
    n, m, k = [int(n), int(m), int(k)]
    track = []
    for track_i in xrange(k):
        track_temp = map(int,raw_input().strip().split(' '))
        track.append(track_temp)
    result = gridlandMetro(n, m, k, track)
    print result
