#!/bin/python

import sys

def legitPoz(x, y, n, m):
    return x >= 0 and x < n and y >= 0 and y < m

def connectedCell(matrix, n, m):
    ans = 0
    viz = []
    # Initialize viz matrix
    for i in range(n):
        viz.append([False]*m)
    
    # initialize direction vectors
    dx = [1, 1, 1, 0, 0, -1, -1, -1]
    dy = [1, 0,-1, 1,-1,  1,  0,  1]
    
    #BFS
    for i in range(n):
        for j in range(m):
            if viz[i][j] == False and matrix[i][j] == 1:
                region = 0
                q = [(i,j)]
                viz[i][j] = True
                while len(q) > 0:
                    head = q[0]
                    region += 1
                    x = q[0][0]
                    y = q[0][1]
                    for k in range(8):
                        newX = x + dx[k]
                        newY = y + dy[k]
                        if legitPoz(newX, newY, n, m):
                            if viz[newX][newY] == False and matrix[newX][newY] == 1:
                                q.append((newX, newY))
                                viz[newX][newY] = True
                    q.pop(0)
                if region > ans:
                    ans = region
    return ans
    
    
    # Complete this function

if __name__ == "__main__":
    n = int(raw_input().strip())
    m = int(raw_input().strip())
    matrix = []
    for matrix_i in xrange(n):
        matrix_temp = map(int,raw_input().strip().split(' '))
        matrix.append(matrix_temp)
    result = connectedCell(matrix, n, m)
    print result
