#!/bin/python3

import os
import sys

# Complete the solve function below.

def findMaxMin(arr, d):
    n = len(arr)
    ans = sys.maxsize
    while (n > 0):
        if (n < d):
            return ans
        m = 0
        index = 0
        for i in range(d):
            if arr[i] > m:
                m = arr[i]
                index = i
        arr = arr[index+1:]
        ans = min(ans, m)
        n = len(arr)
        if n == 0:
            return ans

def solve(arr, queries):
    ans = []
    for d in queries:
        ans.append(findMaxMin(arr, d))
    return ans

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    nq = input().split()

    n = int(nq[0])

    q = int(nq[1])

    arr = list(map(int, input().rstrip().split()))

    queries = []

    for _ in range(q):
        queries_item = int(input())
        queries.append(queries_item)

    result = solve(arr, queries)

    fptr.write('\n'.join(map(str, result)))
    fptr.write('\n')

    fptr.close()
