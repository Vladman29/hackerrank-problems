#!/bin/python3

import sys

def getMinimumCost(n, k, c):
    c = sorted(c, reverse=True)
    ans = 0
    for i in range(n):
        x = i // k
        ans = ans + ( (x+1) * c[i])
    return ans
    
    # Complete this function

n, k = input().strip().split(' ')
n, k = [int(n), int(k)]
c = list(map(int, input().strip().split(' ')))
minimumCost = getMinimumCost(n, k, c)
print(minimumCost)
