#!/bin/python

import sys


g = int(raw_input().strip())
for a0 in xrange(g):
    n = int(raw_input().strip())
    arr = map(int, raw_input().split())
    maxim = 0
    num_turns = 0
    for x in arr:
        if x > maxim:
            maxim = x
            num_turns += 1
    if num_turns % 2 == 0:
        print ("ANDY")
    else:
        print("BOB")
