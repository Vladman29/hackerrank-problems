#!/bin/python

# Enter your code here. Read input from STDIN. Print output to STDOUT
import sys 

from __future__ import print_function
q = int(raw_input().strip())
for qx in range(q):
    n,m = map(int,raw_input().split())
    nei = []
    nei.append([])
    dist = []
    dist.append(-1)
    for i in range(n):
        nei.append([])
        dist.append(-1)
    for i in range(m):
        x,y = map(int,raw_input().split())
        nei[x].append(y)
        nei[y].append(x)
    s = int(raw_input())
    q = []
    q.append(s)
    dist[s] = 0
    while(len(q) > 0):
        cur = q[0]
        for pos in nei[cur]:
            if(dist[pos] == -1):
                dist[pos] = dist[cur] + 6
                q.append(pos)
        q.remove(q[0])
    for i in range(1,n+1):
        if not i == s:
            print(dist[i], end = " ")
    print ("\n",end = "")
   