#!/bin/python3

import sys
import heapq

def prims(n, edges, start):
    graph = {}
    weight = {}
    viz = {}
    for i in range(1,n+1):
        graph[i] = []
        weight[i] = -1
        viz[i] = False
    for (x, y, cost) in edges:
        graph[x].append((y,cost))
        graph[y].append((x,cost))
    weight[start] = 0
    q = []
    heapq.heappush(q,(0, start))
    while len(q) > 0:
        #print(q)
        head = q[0]
        heapq.heappop(q)
        if(viz[head[1]] == True):
            continue
        viz[head[1]] = True
        for (z, cost) in graph[head[1]]:
            if weight[z] == -1:
                weight[z] = cost
                heapq.heappush(q,(cost, z))
            elif weight[z] > cost and not viz[z]:
                weight[z] = cost
                heapq.heappush(q, (cost, z))                    
    answer = 0
    for x in weight.keys():
        answer += weight[x]
    return answer
    # Complete this function

if __name__ == "__main__":
    n, m = input().strip().split(' ')
    n, m = [int(n), int(m)]
    edges = []
    for edges_i in range(m):
        edges_t = [int(edges_temp) for edges_temp in input().strip().split(' ')]
        edges.append(edges_t)
    start = int(input().strip())
    result = prims(n, edges, start)
    print(result)
