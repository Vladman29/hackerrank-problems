#!/bin/python3

import os
import sys
import math

#
# Complete the xorMatrix function below.
# n = 4,8 goes to 0 0 0 0 after couple of steps
# n = 5, loops after k = 15 steps row2 = row 17
# n = 7, loops after k = 7 steps  row2 = row9
# n = 6, loops after k = 5 steps  row3 = row8
# n = 9, loops after k = 63 steps row2 = row65 
# n = 11, loops after k = 341 steps

def findLog(m):
    print(m)
    if m == 0:
        return -1
    if m == 1:
        return 0
    else:
        return 1 + findLog(m//2)

def xorMatrix(m, n, first_row):
    #
    # Write your code here.
    #
    m = m - 1
    next_row = []
    while (m > 0):
        l = findLog(m)
        p = (1<<l)
        next_row = []
        for i in range(n):
            next_row.append(first_row[i] ^ first_row[(i+p)%n])
        first_row = next_row
        m = m - p
    return first_row

def XorMatrixSlow(m, n, first_row):
    m = m-1
    while (m > 0):
        next_row = []
        for i in range(n):
            next_row.append(first_row[i] ^ first_row[(i+1)%n])
        first_row = next_row
        m = m - 1
    return first_row

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    nm = input().split()

    n = int(nm[0])

    m = int(nm[1])

    first_row = list(map(int, input().rstrip().split()))

    result = xorMatrix(m,n, first_row)

    fptr.write(' '.join(map(str, result)))
    fptr.write('\n')

    fptr.close()
