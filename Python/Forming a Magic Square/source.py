#!/bin/python

import sys

def formingMagicSquare(s):
    # Complete this function
    # i j k
    # a 5 b
    # c d e
    magics = []
    for i in range(1,10):
        if i == 5:
            continue
        for j in range(1,10):
            if j == 5 or j == i:
                continue
            k = 15 - i - j
            if k == 5 or k == i or k == j:
                continue
            #print(i,j,k)
            for a in range(1,10):
                if a == 5 or a == k or a == i or a == j:
                    continue
                b = 10 - a
                if b == i or b == j or b == k:
                    continue
                c = 15 - a - i
                if c == b or c == a or c == i or c == j or c == k or c == 5:
                    continue
                d = 10 - j
                if d == b or d == c or d == i or d == k or d == a:
                    continue
                e = 15 - c - d
                if e == a or e == b or e == c or d == e or e == i or e == 5 or e == j or e == k:
                    continue
                if k + b + e == 15 and i + e == 10 and c + k == 10:
                    magics.append([i,j,k,a,5,b,c,d,e])
    ans = 100
    for magic in magics:
        cost = 0
        for i in range(9):
            cost = cost + abs(magic[i] - s[i/3][i%3])
        if cost < ans:
            ans = cost
    return ans
                        
                        
                            


if __name__ == "__main__":
    s = []
    for s_i in xrange(3):
        s_temp = map(int,raw_input().strip().split(' '))
        s.append(s_temp)
    result = formingMagicSquare(s)
    print result
