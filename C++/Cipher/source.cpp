#include <bits/stdc++.h>

using namespace std;

string cipher(int k, string s) {
    // Complete this function
    int i;
    int numofones = 0, n = s.length();
    string ans;
    ans.append(s,0,1);
    for (i=1; i<= n-k; i++)
    {
        if (i >= k)
        {
            if(s[i-1] == s[i])
            {
                ans.append(ans,i-k,1);
            }
            else
            {
                if (ans[i-k] == '0')
                    ans.append("1");
                if (ans[i-k] == '1')
                    ans.append("0");
            }
        }
        else
        {
            if(s[i-1] != s[i])
            {
                ans.append("1");
            }
            else
            {
                ans.append("0");
            }
        }
    }
    return ans;
}

int main() {
    int n;
    int k;
    cin >> n >> k;
    string s;
    cin >> s;
    string result = cipher(k, s);
    cout << result << endl;
    return 0;
}
