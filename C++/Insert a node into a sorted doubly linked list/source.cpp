/*
    Insert Node in a doubly sorted linked list 
    After each insertion, the list should be sorted
   Node is defined as
   struct Node
   {
     int data;
     Node *next;
     Node *prev;
   }
*/
Node* SortedInsert(Node *head,int data)
{
    if(head == NULL)
    {
        Node* n = new Node();
        n -> prev = NULL;
        n -> next = NULL;
        n -> data = data;
        return n;
    }
    if(head->data >= data){
        Node* n = new Node();
        n -> prev = head->prev;
        n -> next = head;
        n -> data = data;
        head -> prev = n;
        return n;
    }
    else{
        head->next = SortedInsert(head->next, data);
        return head;
    }
    // Complete this function
   // Do not write the main method. 
}
