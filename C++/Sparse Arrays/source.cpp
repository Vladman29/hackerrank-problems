#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <cstring>
#include <unordered_map>
using namespace std;


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    unordered_map<string, int> m;
    int n;
    cin >> n;
    for(int i=1; i<=n; i++)
    {
        string x;
        cin >> x;
        if (m.find(x) == m.end())
        {
            m[x] = 1;
        }
        else
        {
            m[x] ++;
        }
    }
    int q;
    cin >> q;
    for (int i=1; i<=q; i++)
    {
        string x;
        cin >> x;
        cout<<m[x]<<'\n';
    }
    
    return 0;
}
