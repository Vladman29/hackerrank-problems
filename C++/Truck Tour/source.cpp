#include <bits/stdc++.h>

using namespace std;

/*
 * Complete the truckTour function below.
 */
int truckTour(vector<vector<int>> petrolpumps) {
    /*
     * Write your code here.
     */
    int n,i,start,curr;
    n = petrolpumps.size();
    start = 0;
    i = 0;
    curr = 0;
    while (true)
    {
        curr = curr + petrolpumps[i][0] - petrolpumps[i][1];
        if(curr < 0)
        {
            curr = 0;
            start = i + 1;
        }
        else
        {
            if ((i + 1) % n == start)
            {
                return start;
            }
        }
        i = (i + 1) % n;
    }

}

int main()
{
    ofstream fout(getenv("OUTPUT_PATH"));

    int n;
    cin >> n;
    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    vector<vector<int>> petrolpumps(n);
    for (int petrolpumps_row_itr = 0; petrolpumps_row_itr < n; petrolpumps_row_itr++) {
        petrolpumps[petrolpumps_row_itr].resize(2);

        for (int petrolpumps_column_itr = 0; petrolpumps_column_itr < 2; petrolpumps_column_itr++) {
            cin >> petrolpumps[petrolpumps_row_itr][petrolpumps_column_itr];
        }

        cin.ignore(numeric_limits<streamsize>::max(), '\n');
    }

    int result = truckTour(petrolpumps);

    fout << result << "\n";

    fout.close();

    return 0;
}
