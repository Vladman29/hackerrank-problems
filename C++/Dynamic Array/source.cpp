#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    int i,n,q;
    cin >> n >> q;
    vector<vector <int>> a;
    vector<int> v;
    for (i = 1; i<=n; i++){
        a.push_back({});
    }
    int last_answer = 0;
    for(i = 1; i<=q ; i++)
    {
        int op,x,y;
        cin >> op >> x >> y;
        int index = (x ^ last_answer) % n;
        if (op == 1)
        {
            a[index].push_back(y);
            continue;
        }
        if (op == 2)
        {
            last_answer = a[index][y % a[index].size()];
            cout << last_answer << endl;
        }
    }
    return 0;
}
