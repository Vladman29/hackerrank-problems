#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <stack>
#include <cstring>
using namespace std;


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    int n,op,i;
    stack <string> st;
    cin >> n;
    for (i=0; i<n; i++)
    {
        cin >> op;
        if (op == 1)
        {
            string w;
            string l;
            cin >> w;
            if (st.empty())
            {
                st.push(w);
            }
            else
            {
                l = st.top();
                l.append(w);
                st.push(l);
            }
        }
        if (op == 2)
        {
            int k;
            string w;
            cin >> k;
            w = st.top();
            w.erase(w.end()-k, w.end());
            st.push(w);
        }
        if (op == 3)
        {
            int k;
            cin >> k;
            string w;
            w = st.top();
            cout << w[k-1] << endl;
        }
        if (op == 4)
        {
            st.pop();
            /*if (!st.empty()){
                cout << st.top()<<endl;
            }
            else
            {
                cout << "EMPTY"<<endl;
            }*/
        }
    }
    return 0;
}
