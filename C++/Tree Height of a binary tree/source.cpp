
/*The tree node has data, left child and right child 
class Node {
    int data;
    Node* left;
    Node* right;
};

*/
    int maxim(int a, int b){
        if (a < b){
            return b;
        }
        else
            return a;
    }
    int height(Node* root) {
        // Write your code here.
        if (root == NULL){
            return -1;
        }
        return 1 + maxim(height(root->left), height(root->right));
    }
  