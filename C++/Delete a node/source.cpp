/*
  Delete Node at a given position in a linked list 
  Node is defined as 
  struct Node
  {
     int data;
     struct Node *next;
  }
*/
Node* Delete(Node *head, int position)
{
  if (position == 0){
      Node* temp = head->next;
      delete(head);
      return temp;
  }
  if (position == 1){
      head->next = Delete(head->next, position-1);
      return head;
  }
  head->next = Delete(head->next, position-1);
  return head;
    // Complete this method
}
