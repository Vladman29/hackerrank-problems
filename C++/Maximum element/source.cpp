#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <stack>
using namespace std;


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */   
    stack <int> st;
    stack <int> st2;
    int n,i, counter=0,x,op;
    cin >> n;
    for (i=0; i<n; i++){
        cin >> op;
        if(op == 1){
            cin >> x;
            st2.push(x);
            if (st.empty()){
                st.push(x);
            }
            else if(st.top() <= x){
                st.push(x);
            }
        }
        if (op == 2){
            if (st.top() == st2.top()){
                st.pop();
            }
            st2.pop();
        }
        if (op == 3){
            cout << st.top()<<endl;
        }
    }
    return 0;
}
