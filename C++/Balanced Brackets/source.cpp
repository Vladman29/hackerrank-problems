#include <bits/stdc++.h>
#include <stack>
using namespace std;


string isBalanced(string s) {
    stack<char> st;
    for (char c:s){
        if (c == '{' || c == '(' || c == '['){
            st.push(c);
            continue;
        }
        else
        {
            if (st.empty()){
                return "NO";
            }
            if (c == '}')
            {
                char t = st.top();
                if (t == '{'){
                    st.pop();
                    continue;
                }
                else{
                    return "NO";
                }
            }
            if (c == ']')
            {
                char t = st.top();
                if (t == '['){
                    st.pop();
                    continue;
                }
                else {
                    return "NO";
                }
            }
            if (c == ')'){
                char t = st.top();
                if (t == '('){
                    st.pop();
                    continue;
                }
                else {
                    return "NO";
                }
            }
        }
    }
    if (st.empty()){
        return "YES";
    }
    return "NO";
    // Complete this function
}

int main() {
    int t;
    cin >> t;
    for(int a0 = 0; a0 < t; a0++){
        string s;
        cin >> s;
        string result = isBalanced(s);
        cout << result << endl;
    }
    return 0;
}
