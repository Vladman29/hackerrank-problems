import heapq
import os, sys

n = int(input())
h = []
toDelete = []
for t in range(n):
    line = input()
    line = line.split()
    #print(line)
    if len(line) > 1:
        op = int(line[0])
        x = int(line[1])
        if op == 1:
            heapq.heappush(h,x)
        if op == 2:
            toDelete.append(x)
    else:
        while h[0] in toDelete:
            heapq.heappop(h)
        print(h[0])