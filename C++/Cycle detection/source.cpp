/*
Detect a cycle in a linked list. Note that the head pointer may be 'NULL' if the list is empty.

A Node is defined as: 
    struct Node {
        int data;
        struct Node* next;
    }
*/

bool has_cycle(Node* head) {
    if ( head == NULL)
    {
        return 0;
    }
    if (head->next == NULL) {
        return 0;
    }
    Node* slow = head;
    Node* fast = head -> next;
    while(fast != NULL){
        fast = fast -> next;
        if (fast == NULL){
            return 0;
        }
        fast = fast -> next;
        slow = slow -> next;
        if (fast == slow){
            return 1;
        }
    }
    return 0;
    // Complete this function
    // Do not write the main method
}
