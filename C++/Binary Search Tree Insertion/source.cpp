/*
Node is defined as 

typedef struct node
{
   int data;
   node * left;
   node * right;
}node;

*/


node * insert(node * root, int value) {
    if( root == NULL){
        node* toInsert = new node;
        toInsert -> data = value;
        toInsert -> left = NULL;
        toInsert -> right = NULL;
        return toInsert;
    }
    node* aux = root; 
   while(aux)
   {
       if(aux->data > value)
       {
           if(aux->left)
           {
               aux = aux -> left;
           }
           else
           {
               node* toInsert = new node;
               toInsert -> data = value;
               toInsert -> left = NULL;
               toInsert -> right = NULL;
               aux ->left = toInsert;
               break;
           }
       }    
       else
       {
           if(aux->right)
           {
               aux = aux ->  right;
           }
           else
           {
               node* toInsert = new node;
               toInsert -> data = value;
               toInsert -> left = NULL;
               toInsert -> right = NULL;
               aux ->right = toInsert;
               break;
           }
       }
   }
   return root;
}
