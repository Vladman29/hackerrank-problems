#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <stack>
using namespace std;


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */ 
    stack <int> st1;
    stack <int> st2;
    int t,i,op,x;
    cin >> t;
    for (i=0; i<t; i++)
    {
        cin >> op;
        if (op == 1)
        {
            cin >> x;
            st1.push(x);
        }
        else
        {
            if(!st2.empty())
            {
                if (op==3)
                {
                    cout << st2.top() << endl;
                }
                else
                {
                    st2.pop();
                }
            }
            else
            {
                while(!st1.empty())
                {
                    st2.push(st1.top());
                    st1.pop();
                }
                if (op==3)
                {
                    cout << st2.top() << endl;
                }
                else
                {
                    st2.pop();
                }
            }
        }
            
    }
    return 0;
}
