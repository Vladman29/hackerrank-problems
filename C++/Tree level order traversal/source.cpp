
/*
struct node
{
    int data;
    node* left;
    node* right;
}*/

#include <queue>

void levelOrder(node * root) {
    queue<node*> q;
    q.push(root);
    while(!q.empty()){
        node* head = q.front();
        if (head->left != NULL){
            q.push(head->left);
        }
        if (head->right != NULL){
            q.push(head->right);
        }
        cout << (head->data) << " ";
        q.pop();
    }
}
